const textFirstName = document.querySelector('#txt-first-name');

const textLastName = document.querySelector('#txt-last-name');

const spanFullName =document.querySelector('#span-full-name');

const updateFullName = () => {

	spanFullName.innerHTML = textFirstName.value + ' ' + textLastName.value;
}

textFirstName.addEventListener('keyup', updateFullName);
textLastName.addEventListener('keyup', updateFullName);